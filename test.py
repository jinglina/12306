# -*- coding: utf-8 -*-
import subprocess
import time

# 获取程序的PID
def get_process_pid():
    '''
    :param  nvidia-smi cmd
    :return:  process_pid
    :type: list
    '''

    # According to the display card command plus parameters, get the program PID
    # 根据显卡命令加参数，获取程序的PID
    #Process_pid_cmd = "nvidia-smi |grep vas |awk '{print $3}'"
    Process_pid_cmd = "nvidia-smi --query-compute-apps=pid --format=csv,noheader"
    Process_pid = subprocess.Popen(Process_pid_cmd,shell=True,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
    return Process_pid

while True:
    try:
        # Declare the list that PID receives
        # 声明PID接收的列表
        pid_list = []
        pro_pid = get_process_pid()
        for i in pro_pid.stdout.readlines():
            pid_list.append(i)
        # 声明接收数据变量
        programinfo = ""
        for i in range(len(pid_list)):
            # lsof -p 5511 |grep /tmp/vas|awk -F'.' 'NR==1{print $2}'

            # Command to find deploymentname
            # 查找deploymentname的命令
           #  deployment_name_cmd = "lsof -p %s |grep -E '/tmp/ev_algo_server'|awk -F'.' 'END{print $2}'" % pid_list[
           #     i].decode("utf-8").rstrip("\n")
            deployment_name_cmd = "docker inspect -f '{{.Config.Hostname}}' $(cat /proc/%s/cgroup | awk -F '-' '{print $NF}'|head -1|awk -F '.' '{print $1}')" % pid_list[i].decode("utf-8").rstrip("\n")

            # Get the deployment name of pod according to the PID found
            # 根据查询到的PID获取POD的deploymentname
            deployment_name = subprocess.Popen(deployment_name_cmd, shell=True, stdout=subprocess.PIPE,
                                               stderr=subprocess.PIPE)
            # get deployment namespce
            deployment_namespace_cmd = "docker inspect -f '{{index .Config.Labels "io.kubernetes.pod.namespace"}}' $(cat /proc/%s/cgroup | awk -F '-' '{print $NF}'|head -1|awk -F '.' '{print $1}')" % pid_list[i].decode("utf-8").rstrip("\n")
            k8s_namespce = subprocess.Popen(deployment_namespace_cmd, shell=True, stdout=subprocess.PIPE,
                                               stderr=subprocess.PIPE)
            # command to obtain information about a program on the GPU
            # 获取程序在GPU上信息的命令
            program_gpu_cmd = "nvidia-smi pmon -s um -d 1 -c 1 |grep %s |awk '{print $1,$5,$8}'" % pid_list[i].decode(
                "utf-8").rstrip("\n")
           

            # Get program information on the GPU
            # 获取程序在GPU上信息： utilization.memory   utilization.gpu  unit： %
            program_gpu_data = subprocess.Popen(program_gpu_cmd, shell=True, stdout=subprocess.PIPE,
                                                stderr=subprocess.PIPE)

            # return None or 9730-14500-vas-nqp2
            k8s_deploymentname = ""
            k8s_podname = ""
            namespace = ""
            for namespace in k8s_namespce.stdout.readlines():
                namespaace = namespace.decode("utf-8")
            for deployment in deployment_name.stdout.readlines():
                # Remove redundant strings and formats
                # 去除多余的字符串和格式
                k8s_deploymentname = deployment.decode("utf-8").rstrip("\n").rsplit("-", 2)[0]
                k8s_podname = deployment.decode("utf-8")

            # assignment
            # 赋值
            program_data = {}
            for gpu_data in program_gpu_data.stdout.readlines():
                gpuindex = gpu_data.decode("utf-8").rstrip("\n").split(" ")[0]
                program_data["program.utilization.gpu"] = gpu_data.decode("utf-8").rstrip("\n").split(" ")[1]
                if gpu_data.decode("utf-8").rstrip("\n").split(" ")[0] == "-":
                    program_data["program.utilization.gpu"] = 0
                program_data["program.memory.used"] = gpu_data.decode("utf-8").rstrip("\n").split(" ")[2]
                 
                # 定义prometheus的数据格式
                # ProgramInfo{index="9690-12887-vas-jjpm",argu="program.utilization.gpu"} 0
                temp = '{Name}{{k8s_deploymentname="{index}",k8s_podname="{podname}",k8s_namespace="{ns}",gpuindex="{id}",argu="{lable}"}} {value}\n'
                if k8s_deploymentname:
                   for pk, pv in program_data.items():
                       values = {"Name": "ProgramInfo", "id":gpuindex, "index": k8s_deploymentname, "podname": k8s_podname, "ns": namespace, "lable": pk, "value": pv}
                       programinfo = programinfo + temp.format(**values)

        with open('/home/program-test.prom', 'w') as program:
            program.write(programinfo)
        program.close()
    except Exception:
        print("nvidia-smi no fountd!!")
    time.sleep(3)
